import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import { ToDo } from '../models/todo';
import { connect } from 'react-redux';
import { createToDo } from '../models/reducer';

class CreateToDo extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = { name : '', description : '' };
    }
    
    createNewToDo() {
        this.props.createToDo(new ToDo(this.state.name, this.state.description));
        this.props.navigation.navigate('ToDoList');
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput style={ styles.input } placeholder="Name" onChangeText={ (name) => this.setState({ name }) } />
                <TextInput style={ styles.input } placeholder="Description" onChangeText={ (description) => this.setState({ description }) } />
                <Button title="Create" onPress={ () => this.createNewToDo() }/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: 'flex-start',
        padding: 10
    },
    input : {
        height : 50,
        fontSize : 20,
        marginBottom : 10
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = {
    createToDo
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateToDo);
import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { getToDos, selectToDo } from '../models/reducer';
import { ToDo } from '../models/todo';

class ToDoList extends React.Component {
    
    componentDidMount() {
        this.props.getToDos();
    }

    viewToDoDetail(todo) {
        this.props.selectToDo(todo);
        this.props.navigation.navigate('UpdateToDo');
    }
    
    render() {
        return (
            <View style={ styles.container }>
                <FlatList data={this.props.todos} renderItem={({ item }) =>  <Text onPress={ () => this.viewToDoDetail(item) } style={ [styles.item, item.done ? styles.completed : null] }>{ item.name }</Text> } />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    item : {
        padding : 20,
        fontSize : 20,
        color : 'red'
    },
    completed : {
        color : 'green',    
        textDecorationLine : 'line-through'
    }
});

const mapStateToProps = state => {
    return {
        todos : state.todos.map(todo => ({ ...todo, key : todo.id }))
    }
};

const mapDispatchToProps = {
    getToDos,
    selectToDo
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);
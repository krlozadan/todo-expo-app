import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, View, Switch } from 'react-native';
import { updateToDo } from '../models/reducer';

class UpdateToDo extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.todoTitle}>{ this.props.todo.name }</Text>
                <Text style={styles.todoDescription}>{ this.props.todo.description }</Text>
                <Text>Is it Done?</Text>
                <Switch onValueChange={ () => { this.props.updateToDo() } } value={ this.props.todo.done }/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding : 10,
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: 'flex-start',
    },
    todoTitle : {
        fontSize : 20
    },
    todoDescription : {
        marginTop : 10,
        marginBottom : 10
    }
});

const mapStateToProps = state => {
    console.log(state);
    return {
        todo : state.selectedToDo
    };
};

const mapDispatchToProps = {
    updateToDo
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateToDo);
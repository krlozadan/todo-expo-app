export class ToDo {
    constructor(name, description) {
        this.name = name;
        this.description = description;
        this.done = false;
        this.id = Date.now().toString();
    }
}
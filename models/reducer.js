import { ToDo } from './todo';
// Actions
const GET_TODOS = 'my-first-expo-app/todos/GET';
const CREATE_TODO = 'my-first-expo-app/todos/CREATE';
const SELECT_TODO = 'my-first-expo-app/todos/SELECT';
const UPDATE_TODO = 'my-first-expo-app/todos/UPDATE';

// Reducer
export default function reducer(state = { todos : [new ToDo('dskjfh', 'dkfhdsh')], selectedToDo : null }, action) {
    switch (action.type) {
        case GET_TODOS : 
            return { ...state };
        case CREATE_TODO : 
            return { ...state, todos : [...state.todos, action.todo] };
        case SELECT_TODO : 
            return { ...state, selectedToDo : action.todo };
        case UPDATE_TODO : 
            let todoToBeUpdated = { ...state.selectedToDo };
            todoToBeUpdated.done = !todoToBeUpdated.done;
            let allToDos = state.todos.map(todo => {
                if (todo.id === todoToBeUpdated.id) {
                    todo.done = !todo.done;
                }
                return todo;
            });
            return { todos : allToDos, selectedToDo : todoToBeUpdated };
        default: 
            return state;
    }
}

// Action creators
export function getToDos() {
    return { type : GET_TODOS }
}

export function createToDo(todo) {
    return { type : CREATE_TODO, todo }
}

export function selectToDo(todo) {
    return { type : SELECT_TODO, todo }
}

export function updateToDo() {
    return { type : UPDATE_TODO }
}
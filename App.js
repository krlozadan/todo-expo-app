import React from 'react';
import { Button, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducer from './models/reducer';

// Screens
import ToDoList from './screens/ToDoList';
import CreateToDo from './screens/CreateToDo';
import UpdateToDo from './screens/UpdateToDo';

const store = createStore(reducer);

const Stack = createStackNavigator({
    ToDoList: {
        screen: ToDoList,
        navigationOptions: ({ navigation }) => ({
            title: 'My To Do List',
            headerRight: ( <Button onPress = { () => navigation.navigate('CreateToDo') } title="+ ToDo" /> )
        })
    },
    CreateToDo: {
        screen: CreateToDo,
        navigationOptions: ({ navigation }) => ({
            title: 'Create To Do',
        })
    },
    UpdateToDo: {
        screen: UpdateToDo,
        navigationOptions: ({ navigation }) => ({
            title: 'Update To Do',
        })
    }
});

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <View style={{ flex: 1, backgroundColor: '#fff' }}>
                    <Stack />
                </View>
            </Provider>
        );
    }
}